var CalculatorModule = (function () {
	
	function extend(Child, Parent) {
		var F = function() { };
		F.prototype = Parent.prototype;
		Child.prototype = new F();
		Child.prototype.constructor = Child;
		Child.superclass = Parent.prototype;
	};
	
	function Calculator(){
	};

	Calculator.prototype.plus = function(operand1,operand2){
		return Number(operand1) + Number(operand2);
	};
	
	Calculator.prototype.minus = function(operand1,operand2){
		return operand1 - operand2;
	};
	
	Calculator.prototype.multiply = function(operand1,operand2){
		return operand1 * operand2;
	};
	
	Calculator.prototype.divide = function(operand1,operand2){
		return operand1 / operand2;
	};
	
	Calculator.prototype.proc = function(operand1,operand2){
		return operand1 % operand2;
	};
	
	Calculator.prototype.increment = function(operand){
		return ++operand;
	};

	Calculator.prototype.decrement = function(operand){
		return --operand;
	};
	
	Calculator.prototype.negative = function(operand){
		return operand * (-1);
	};
	
	function AdvancedCalculator(){
	};
	
	//1 variant
	//extend(AdvancedCalculator, Calculator);
	
	//2 variant
	AdvancedCalculator.prototype = Object.create(Calculator.prototype);
	
	
	AdvancedCalculator.prototype.cos = function(operand){
		return Math.acos(operand);
	};
	
	AdvancedCalculator.prototype.sin = function(operand){
		return Math.asin(operand);
	};
	
	AdvancedCalculator.prototype.sqrt = function(operand){
		return Math.sqrt(operand);
	};
	
	AdvancedCalculator.prototype.pow = function(operand1,operand2){
		return Math.pow(operand1,operand2);
	};
	
	AdvancedCalculator.prototype.abs = function(operand1){
		return Math.abs(operand1);
	};
	
	AdvancedCalculator.prototype.exp = function(operand1){
		return Math.exp(operand1);
	};
	
	var advancedCalc = new AdvancedCalculator();
	
	var simpleCalc = new Calculator();
	
	return {
			simpleCalc: simpleCalc,
			advancedCalc: advancedCalc
	};
})();
