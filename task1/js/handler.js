var CalculatorModule = CalculatorModule || {};

window.onload = function () {
	document.querySelector('#plus').addEventListener("click", function(ev) {
		var op1 = document.getElementById('operand1').value;
		var op2 = document.getElementById('operand2').value;
		document.getElementById('result').value = CalculatorModule.advancedCalc.plus(op1,op2);
	});	

	document.querySelector('#minus').addEventListener("click", function(ev) {
		var op1 = document.getElementById('operand1').value;
		var op2 = document.getElementById('operand2').value;
		document.getElementById('result').value = CalculatorModule.advancedCalc.minus(op1,op2);
	});	

	document.querySelector('#divide').addEventListener("click", function(ev) {
		var op1 = document.getElementById('operand1').value;
		var op2 = document.getElementById('operand2').value;
		document.getElementById('result').value = CalculatorModule.advancedCalc.divide(op1,op2);
	});

	document.querySelector('#multiply').addEventListener("click", function(ev) {
		var op1 = document.getElementById('operand1').value;
		var op2 = document.getElementById('operand2').value;
		document.getElementById('result').value = CalculatorModule.advancedCalc.multiply(op1,op2);
	});

	document.querySelector('#sin').addEventListener("click", function(ev) {
		var op1 = document.getElementById('operand1').value;
		document.getElementById('result').value = CalculatorModule.advancedCalc.sin(op1);
	});	

	document.querySelector('#cos').addEventListener("click", function(ev) {
		var op1 = document.getElementById('operand1').value;
		document.getElementById('result').value = CalculatorModule.advancedCalc.cos(op1);
	});	

	document.querySelector('#sqrt').addEventListener("click", function(ev) {
		var op1 = document.getElementById('operand1').value;
		document.getElementById('result').value = CalculatorModule.advancedCalc.sqrt(op1);
	});

	document.querySelector('#pow').addEventListener("click", function(ev) {
		var op1 = document.getElementById('operand1').value;
		var op2 = document.getElementById('operand2').value;
		document.getElementById('result').value = CalculatorModule.advancedCalc.pow(op1,op2);
	});

	document.querySelector('#proc').addEventListener("click", function(ev) {
		var op1 = document.getElementById('operand1').value;
		var op2 = document.getElementById('operand2').value;
		document.getElementById('result').value = CalculatorModule.advancedCalc.proc(op1,op2);
	});
	document.querySelector('#abs').addEventListener("click", function(ev) {
		var op = document.getElementById('operand1').value;
		document.getElementById('result').value = CalculatorModule.advancedCalc.abs(op);
	});
	document.querySelector('#exp').addEventListener("click", function(ev) {
		var op = document.getElementById('operand1').value;
		document.getElementById('result').value = CalculatorModule.advancedCalc.exp(op);
	});
	document.querySelector('.negative').addEventListener("click", function(ev) {
		var op;
		if(document.getElementById('checkOperand').checked) {
			op = document.getElementById('operand1').value;
			document.getElementById('operand1').value=CalculatorModule.advancedCalc.negative(op);
		} else {
			op = document.getElementById('operand2').value;
			document.getElementById('operand2').value=CalculatorModule.advancedCalc.negative(op);
		}
		
		
	});

	document.querySelector('.clear').addEventListener("click", function(ev) {
		document.getElementById('operand1').value="";
		document.getElementById('operand2').value="";
		document.getElementById('result').value = "";
	});

	function listener(){
		var elements = document.querySelectorAll(".numeral");
		var i = 0;
		for(; i < elements.length; i++){
			elements[i].addEventListener("click", function(ev){
				var val = this.innerHTML;
				if(document.getElementById('checkOperand').checked) {
					document.getElementById('operand1').value=document.getElementById('operand1').value + val;
				} else {
					document.getElementById('operand2').value=document.getElementById('operand2').value + val;
				}
			}
			);
		}
	};
	listener();
	document.querySelector('#operand1').addEventListener("click", function(ev) {
		document.getElementById('checkOperand').checked="checked";
	});

	document.querySelector('#operand2').addEventListener("click", function(ev) {
		document.getElementById('checkOperand').checked="";
	});

};